# Ambiente de desenvolvimento

Neste repositório temos o arquivo de configuração do `docker-compose` para execução das aplicações e banco de dados em ambiente local utilizando as imagens docker publicadas no Docker Hub.

## Docker-compose

Para executar o projeto digite o comando abaixo:

```bash
docker-compose up -d
```

Agora devemos criar os bancos de dados. Para isso, é necessário executar o script `create-databases.sh`. Este script está dentro do [projeto backend]().

```bash
./create-databases.sh
````